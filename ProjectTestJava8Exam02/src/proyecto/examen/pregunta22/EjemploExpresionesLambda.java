package proyecto.examen.pregunta22;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class EjemploExpresionesLambda {

	
	public static void main(String[] args) {
		
		
		// imprimir la lista usando labda
		for(int x :  Arrays.asList(1,2,3,4)){
			System.out.println(x);	
		}
		System.out.println("********");
		List<Integer> l = Arrays.asList(1,2,3,4);
		l.forEach( (n) -> System.out.println(n)  );
		
		Arrays.asList(1,'s',"4",3,true).forEach( x -> System.out.println(x) );
		//System.out.println( Arrays.asList(1,2,3,4).contains(5) );
		
		
		// PREDICADOS-> Son expresiones que reciben argumentos y retornan un valor l�gico
		
		List<Integer> listaNumeros = Arrays.asList(1,2,3,4,45,48,46,49);
		System.out.println("numeros pares");
		evaluar(listaNumeros, (x) -> x %2 == 0 );
		
		System.out.println("\nnumeros impares");
		evaluar(listaNumeros, (x) -> x %2 != 0 );
		
		
	}

	public static void evaluar(List<Integer> listanumero, Predicate<Integer> predicado){
		for (Integer integer : listanumero) {
			if(predicado.test(integer)){
				System.out.print(integer + " ");
			}
		}
	}
	
}
