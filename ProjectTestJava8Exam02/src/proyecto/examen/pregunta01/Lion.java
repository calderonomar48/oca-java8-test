package proyecto.examen.pregunta01;


interface Jumpable{}

class Animal{}


public class Lion extends Animal implements Jumpable  {

	
	public static void main(String[] args) {
		
		Animal o = new Animal();
		
		// al hacer esto se est� cayendo en un error de classCastException
		// una clase hija no puede almacenar la instancia de una clase Padre, asi este Padre sea casteado a su clase hija
		// va a lanzar un erro en tiempo de ejecucion
		
		System.out.println("1");
		try {

			// CASO1
			// tratando de almacenar un padre en un hijo, asi se se castee la JVM se va quejar y va asaltar un error en tiempo en tiempo de ejecucion
			//Lion l = (Lion) new Animal();
			
			// CASO2
			// aqui si no queja el compilador, porque est�s almacenando la instancia de un hijo en un padre
			Animal a = new Lion();
			
			// CASO 3
			// el siguiente codigo va a lanzar un error de classcastexception porque est�s tratando de crear un objeto interface que sea de tipo Animal
			// no existe linea de herencia entre Jumpable y Animal, 
			// se podr�a solucionar si a la clase Animal hacemos que implemente Jumpable
			//Jumpable j = (Jumpable) new Animal();
			
			// CASO 4
			// si existe linea de herencia entre Jumpable y Lion, en este caso Lion es el hijo de Jumpable
			// el sgte codigo lanzar� error de classcastexception porque se est� tratando de almacenar la instanci
			Jumpable j = new Lion();
			
			
		} catch (ClassCastException e) {
			System.out.println("2");
			//throw new RuntimeException("r0");
//		} catch (RuntimeException r){
//			System.out.println("3");
			//throw new RuntimeException("r1");
		} finally {
			System.out.println("4");
		}
		System.out.println("5");
		
	}
}