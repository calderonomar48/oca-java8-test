package proyecto.examen.pregunta05;

public class ColorPencil extends Pencil {

	
	String color;
	
	public ColorPencil(String color) {
		this.color = color;
	}
	
	public static void main(String[] args) {
		ColorPencil o = new ColorPencil("RED");
		
		Pencil p = new ColorPencil("RED");
		
		System.out.println("llego");
		
		// Pencil es el padre
		// ColorPencil es el hijo
		// no puedes almacenar una instancia de un objeto padre en un hijo
		// si haces un casteo explicito la JVM ya no te dar� error
		// pero si saldr� un error en tiempo de ejecuci�n del programa
		ColorPencil c = (ColorPencil) new Pencil();
	}
	
}

class Pencil{
}
