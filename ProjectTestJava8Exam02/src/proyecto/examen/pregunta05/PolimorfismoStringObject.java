package proyecto.examen.pregunta05;

public class PolimorfismoStringObject {



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// po
		Integer num = 4;
		Number n1 = num;
		Integer k = (Integer) n1;
		Number nn = (Number) k;
		
		
		System.out.println("1");
		
		// polimorfismo de String a Object
		Object o = new String("s");
		System.out.println(o);
		
		String str = (String) o;
		System.out.println(o);
		
		// polimorfismo entre clases
		A l = new A();
		
		A aa = new B();
		
		B bb = (B) aa;
		
		System.out.println("fin");
	}
}


class A { }
class B extends A{ }