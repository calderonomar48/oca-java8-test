package proyecto.examen.pregunta11;

public class Handset {

	public static void main(String[] args) {
		
		double price = 0d;
		String model = " ";
		
		Keys varKeys = (a, b) -> { if( b >=32 ) return a; else return "default"; };
		// no compila el codigo porque model ni price no han sido iniclizados
		System.out.println( model + price + varKeys.keypad("PERU", 32));
	
		String c = "dasdas /* sdad */ sdsd";
		System.out.println(c);
		
	}
	
}


interface Keys{
	String keypad (String region, int keys);
}