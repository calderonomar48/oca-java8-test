package proyecto.examen.pregunta10;

public class EJava {

	
	public static void main(String[] args) {
		Person p = new Person();
		// creando al vuelo la implementaci�n del metodo move
		Moveable m = (x) -> Person.MIN_DISTANCE +x;
		// m.move  ->  hace q llame al metodo move de la interface q ya fue implementado en la linea anterior y le pasa como parametro el valor 20
		
		System.out.println(p.name + p.height + p.result + p.age + m.move(20));
		// imprimer null0.0false025
		// name de Person retorna null porque es un Objeto String
		// height retorna 0.0
		// result retorn false porque es su valor primitivo por default, si fuera Boolean retorna null
		// age retorna 0, su valor primitivo retorna 0
		// m.move se implement� en la funci�n lambda, y se le env�a como parametro 20, por lo tanto 5+20=25
	}
}


interface Moveable{
	int move(int distance);
	
}

class Person {
	static int MIN_DISTANCE = 5;
	int age;
	float height;
	boolean result;
	String name;
}
