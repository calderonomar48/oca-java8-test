package proyecto.examen.pregunta10;

import java.util.Arrays;
import java.util.List;

public class TestLambda {

	// las expresiones lambdas son funciones anonimas
	// no se declaran en ninguna parte y se crean al vuelo y cumple un objetivo,
	// se ejcutan y realizan la acci�n y ahi terminan
	// los lambdas son funciones que no necesitan de una clase para su creaci�n
	public static void main(String[] args) {

		// imprimir una lista utilizando expresiones lambda en Java 8
		//Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).forEach(n -> System.out.print(n + " "));
		
		
		//n.forEach( x -> System.out.println(x) );
		
		//int x[] = {1,2,3};
		
		List<Integer> n = Arrays.asList(1,2,3,null);
		
		//n.add(5);
		n.set(3, 5);
		n.set(0, 454564565);
		
		System.out.println(n.size());
		

	}

}
