package proyecto.examen.pregunta42;

import java.util.function.Predicate;

public class Test {

	public static void main(String[] args) {
		
		int a = 10;
		int b = 100;
		
		Predicate<Integer> compare = (var) -> var++ == 10;
		if (b++ >100 && compare.test(a) ) {
			System.out.println(a+b);
		}
		
		
		
		
	}
	
}
