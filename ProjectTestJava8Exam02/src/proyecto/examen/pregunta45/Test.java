package proyecto.examen.pregunta45;

public class Test {

	public static void main(String[] args) {
		
		Phone phone = new Tablet();
		System.out.println(phone.keyboard);
		
		Tablet p = new Tablet();
		System.out.println(p.keyboard);
		System.out.println(p.playMovie);
		
		Phone l = new Phone();
		System.out.println(l.keyboard);
		
		Tablet u = (Tablet) new Phone();
		System.out.println(u.keyboard);
		System.out.println(u.playMovie);
		
		
	}
	
}

class Phone{
	String keyboard = "in-built";
}

class Tablet extends Phone{
	boolean playMovie = false;
}
