package proyecto.examen.pregunta39;

import java.time.LocalDate;

public class TestEquals {

	public static void main(String[] args) {
		
		Book b1 = new Book("1234");
		Book b2 = new Book("123411");
		
		LocalDate l = null;
		l = b1.equals(b2) ? b1 == b2 ? LocalDate.of(2050, 12, 12) : LocalDate.parse("2017-02-01") : LocalDate.parse("9999-09-09") ;
		System.out.println(l);
	}
	
}

class Book{
	String ISBN;
	
	public Book(String val) {
		ISBN = val;
	}
	
	@Override
	public boolean equals(Object obj) {
		System.out.println("isbn " + ISBN);
		System.out.println("(Book) obj " +( (Book) obj).ISBN);
		if (obj instanceof Book) {
			return ( (Book) obj ).ISBN.equals(ISBN);
		} else {
			return false;
		}
		
		
	}
}
