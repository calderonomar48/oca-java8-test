package proyecto.examen.pregunta15.separata.test;

import java.io.IOException;

public class Bunny {

	
	public static void main(String[] args) {
		// tenemos que controlar el exception de tipo NoMoreCarrotsException porque es de tipo
		try {
			eatCarrot();
		} catch (NoMoreCarrotsException e) {
			
		} catch (RuntimeException e) {
		
			// TODO: handle exception
		}
		
	}
	
	public static void eatCarrot() throws NoMoreCarrotsException{
		
	}
	
}

class NoMoreCarrotsException extends Exception{
	
}
